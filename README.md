# pdp-sql-movie

Demo project to work with PostgreSQL and SQL queries

Description of domain:

Entities:
- Movie (ID, Title, Description, Length), relations to Actors, Genre, Reviews, Director
- Genre (ID, name)
- Reviews (Reviwer, Review, Movie, Stars)
- Actors (ID, First / Last Name, Birdth date), relations to Movies
- Roles (ID, ActorID, Role, MovieID) unique for Role + MovieID
- Director (ID First / Last Name, Birdth date), relations to Movie

Requests:
- [ ] List all the information of the actors who played a role in the movie 'MOVIE_NAME'.
- [ ] Write a query in SQL to list all the movies which released in the country other than UK.
- [ ] Write a query in SQL to find the movie title, year, date of release, director and actor for those movies which reviewer is unknown.
- [ ] Write a query in SQL to find the titles of all movies directed by the director whose first and last name are FIRST_NAME LAST_NAME.
- [ ] Write a query in SQL to find all the years which produced at least one movie and that received a review of more than 3 stars. Show the results in increasing order
- [ ] Write a query in SQL to find the titles of all movies that have no reviews
- [ ] Write a query in SQL to find the names of all reviewers who have reviews with a NULL value
- [ ] Write a query in SQL to find the reviewer's name and the title of the movie for those reviewers who rated more than one movies.Sort the result by reviewer name, movie title, and number of stars
- [ ] Write a query in SQL to find the movie title, and the highest number of stars that movie received and arranged the result according to the group of a movie and the movie title appear alphabetically in ascending order
- [ ] Write a query in SQL to find the names of all reviewers who rated the movie MOVIE_NAME.
- [ ] Write a query in SQL to return the reviewer name, movie title, and number of stars for those movies which review is the lowest one.
- [ ] Write a query in SQL to list the first and last names of all the actors who were cast in the movie 'M', and the roles they played in that production.
- [ ] Write a query in SQL to list all the actors who have not acted in any movie between 1990 and 2000.
- [ ] Write a query in SQL to list all the movies with year, genres, and name of the director.
- [ ] Write a query in SQL to find those lowest duration movies along with the year, director's name, actor's name and his/her role in that production.
- [ ] Write a query in SQL to find movie title and number of stars for each movie that has at least one review and find the highest number of stars that movie received and sort the result by movie title
- [ ] Write a query in SQL to find the first and last name of an actor with their role in the movie which was also directed by themselve. 
- [ ] Write a query in SQL to find the highest-rated Mystery movie, and report the title, year, and review

