--DROP DATABASE IF EXISTS movies_db;
--CREATE DATABASE movies_db;
CREATE SCHEMA IF NOT EXISTS movies;

drop table if exists movies.movie_country;
drop table if exists movies.movie_genre;
drop table if exists movies.movie_director;
drop table if exists movies.movie_actor_role;
drop table if exists movies.country;
drop table if exists movies.genre;
drop table if exists movies.director;
drop table if exists movies.actor;
drop table if exists movies.review;
drop table if exists movies.reviewer;
drop table if exists movies.movie;


CREATE TABLE movies.genre (
	genre_id serial PRIMARY KEY,
	genre_name VARCHAR (50) UNIQUE NOT NULL
);

CREATE TABLE movies.country (
	country_id serial PRIMARY KEY,
	country_name VARCHAR (50) UNIQUE NOT NULL
);

CREATE TABLE movies.actor (
	actor_id serial PRIMARY KEY,
	first_name VARCHAR (50) NOT NULL,
	second_name VARCHAR (50),
	birth_date DATE
);

CREATE TABLE movies.director (
	director_id serial PRIMARY KEY,
	first_name VARCHAR (50) NOT NULL,
	second_name VARCHAR (50),
	birth_date DATE
);

CREATE TABLE movies.movie (
	movie_id serial PRIMARY KEY,
	title VARCHAR (100) NOT NULL,
	description VARCHAR (1000) NOT NULL,
	year INT CHECK (year >= 1895) NOT NULL,
	duration TIME NOT NULL
);

CREATE TABLE movies.reviewer (
	reviewer_id serial PRIMARY KEY,
	first_name VARCHAR (50) NOT NULL,
	second_name VARCHAR (50),
	birth_date DATE
);

CREATE TABLE movies.review (
	review_id serial PRIMARY KEY,
	movie_id serial NOT NULL,
	reviewer_id serial NOT NULL,
	rate INT CHECK (rate > 0 AND rate <= 10) NOT NULL,
	CONSTRAINT fk_movie FOREIGN key (movie_id) REFERENCES movies.movie(movie_id),
	CONSTRAINT fk_reviewer FOREIGN key (reviewer_id) REFERENCES movies.reviewer(reviewer_id),
	CONSTRAINT uk_review_reviewer UNIQUE (movie_id, reviewer_id)	
);

CREATE TABLE movies.movie_genre (
	movie_id serial REFERENCES movies.movie(movie_id),
	genre_id serial REFERENCES movies.genre(genre_id),
	CONSTRAINT pk_movie_genre PRIMARY KEY (movie_id, genre_id)
);

CREATE TABLE movies.movie_country (
	movie_id serial REFERENCES movies.movie(movie_id),
	country_id serial REFERENCES movies.country(country_id),
	CONSTRAINT pk_movie_country PRIMARY KEY (movie_id, country_id)
);

CREATE TABLE movies.movie_actor_role (
	movie_id serial REFERENCES movies.movie(movie_id),
	actor_id serial REFERENCES movies.actor(actor_id),
	role_name VARCHAR (50) NOT NULL,
	CONSTRAINT pk_movie_actor PRIMARY KEY (movie_id, actor_id),
	CONSTRAINT uk_movie_actor_role UNIQUE (movie_id, actor_id, role_name)
);

CREATE TABLE movies.movie_director (
	movie_id serial REFERENCES movies.movie(movie_id),
	director_id serial REFERENCES movies.director(director_id),
	CONSTRAINT pk_movie_director PRIMARY KEY (movie_id, director_id)
);

INSERT INTO movies.genre
(genre_id, genre_name)
values
('1', 'Drama'),
('2', 'Crime'),
('3', 'Action'),
('4', 'Adventure'),
('5', 'Biography'),
('6', 'Hystory'),
('7', 'Sci-Fi'),
('8', 'Comedy');

INSERT INTO movies.country
(country_id, country_name)
values
('1', 'United States'),
('2', 'United Kingdom'),
('3', 'New Zealand'),
('4', 'Germany'),
('5', 'Hong Kong');

INSERT INTO movies.actor
(actor_id, first_name, second_name, birth_date)
values
('1', 'Tim', 		'Robbins', 		'1952-10-16'),
('2', 'Morgan', 	'Friman', 		'1937-06-01'),
('3', 'Bob', 		'Gunton', 		'1945-11-15'),
('4', 'Marlon', 	'Brando', 		'1942-04-03'),
('5', 'Al', 		'Pacino', 		'1940-04-25'),
('6', 'James', 		'Caan', 		'1940-03-26'),
('7', 'Christian',  'Bale', 		'1974-01-30'),
('8', 'Heath', 		'Ledger', 		'1979-04-04'),
('9', 'Aaron', 		'Eckhart', 		'1968-03-12'),
('10', 'Robert', 	'De Niro', 		'1943-08-17'),
('11', 'Robert', 	'Duvall', 		'1931-01-05'),
('12', 'Diane', 	'Keaton', 		'1946-01-05'),
('13', 'Henry', 	'Fonda', 		'1905-05-16'),
('14', 'Lee', 		'J. Cobb', 		'1911-12-08'),
('15', 'Martin', 	'Balsam', 		'1919-11-04'),
('16', 'Elijah', 	'Wood', 		'1981-01-28'),
('17', 'Viggo', 	'Mortensen', 	'1958-10-20'),
('18', 'Ian', 		'McKellen', 	'1939-05-25'),
('19', 'John', 		'Travolta', 	'1954-02-18'),
('20', 'Uma', 		'Thurman', 		'1970-04-29'),
('21', 'Samuel', 	'L. Jackson', 	'1948-12-21'),
('22', 'Liam', 		'Neeson', 		'1952-06-07'),
('23', 'Ralph', 	'Fiennes', 		'1962-12-22'),
('24', 'Ben', 		'Kingsley', 	'1943-12-31'),
('25', 'Leonardo', 	'DiCaprio', 	'1974-11-11'),
('26', 'Joseph', 	'Gordon-Levitt','1981-02-07'),
('27', 'Elliot', 	'Page', 		'1987-02-21'),
('28', 'Brad', 		'Pitt', 		'1963-12-18'),
('29', 'Edward', 	'Norton', 		'1969-08-18'),
('30', 'Meat', 		'Loaf', 		'1947-09-27'),
('31', 'Jackie', 	'Chan', 		'1954-04-07'),
('32', 'Maggie', 	'Cheung', 		'1964-09-20'),
('33', 'Brigitte', 	'Lin', 		    '1954-11-03');

INSERT INTO movies.director
(director_id, first_name, second_name, birth_date)
values
('1', 'Frank', 			'Darabont', 	'1959-01-28'),
('2', 'Francis', 		'Ford Coppola', '1939-04-07'),
('3', 'Christopher', 	'Nolan', 		'1970-07-30'),
('4', 'Sidney', 		'Lumet', 		'1924-06-25'),
('5', 'Peter', 			'Jackson', 		'1961-10-31'),
('6', 'Quentin', 		'Tarantino', 	'1963-03-27'),
('7', 'Steven', 		'Spielberg', 	'1946-12-18'),
('8', 'David', 			'Fincher', 		'1962-08-28'),
('9', 'Jackie', 	    'Chan', 		'1954-04-07');

INSERT INTO movies.movie
(movie_id, title, "year", duration, description)
values
('1', 'The Shawshank Redemption', 						1994, '2:22:00', 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.'),
('2', 'The Godfather', 									1972, '2:55:00', 'An organized crime dynasty''s aging patriarch transfers control of his clandestine empire to his reluctant son.'),
('3', 'The Dark Knight', 								2008, '2:32:00', 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.'),
('4', 'The Godfather: Part II', 						1974, '3:22:00', 'The early life and career of Vito Corleone in 1920s New York City is portrayed, while his son, Michael, expands and tightens his grip on the family crime syndicate.'),
('5', '12 Angry Men', 									1957, '1:36:00', 'A jury holdout attempts to prevent a miscarriage of justice by forcing his colleagues to reconsider the evidence.'),
('6', 'The Lord of the Rings: The Return of the King',	2003, '3:21:00', 'Gandalf and Aragorn lead the World of Men against Sauron''s army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.'),
('7', 'Pulp Fiction', 									1994, '2:34:00', 'The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.'),
('8', 'Schindler''s List', 								1993, '3:15:00', 'In German-occupied Poland during World War II, industrialist Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.'),
('9', 'Inception',				 						2010, '2:28:00', 'A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.'),
('10', 'Fight Club', 									1999, '2:19:00', 'An insomniac office worker and a devil-may-care soap maker form an underground fight club that evolves into much more.'),
('11', 'Police Story', 									1985, '1:39:00', 'A virtuous Hong Kong Police Officer must clear his good name when the drug lord he is after frames him for the murder of a dirty cop.');

INSERT INTO movies.reviewer
(reviewer_id, first_name, second_name, birth_date)
values
('1', 'De witt', 	'Goodie', 		'1994-12-21'),
('2', 'Blinnie', 	'Gowen', 		'1990-06-28'),
('3', 'Gail', 		'Bertouloume',	'1989-09-10'),
('4', 'Hugibert', 	'Wilden', 		'2000-01-05'),
('5', 'Michel', 	'Harbard', 		'1987-04-07'),
('6', 'Shari', 		'Starmore', 	'1983-11-15'),
('7', 'Justus', 	'Beetham', 		'1995-04-28'),
('8', 'Ben', 	    'Clapton', 		'1997-12-02'),
('9', 'unknown', 	'unknown', 		null);

INSERT INTO movies.review
(review_id, movie_id, reviewer_id, rate)
values
('1',  '1', '1', 8),
('2',  '1', '2', 7),
('3',  '1', '3', 6),
('4',  '1', '4', 9),
('5',  '1', '5', 10),
('6',  '2', '6', 5),
('7',  '2', '7', 6),
('8',  '2', '1', 4),
('9',  '2', '2', 8),
('10', '3', '4', 9),
('11', '3', '6', 9),
('12', '3', '7', 8),
('13', '4', '1', 4),
('14', '4', '3', 5),
('15', '4', '5', 6),
('16', '4', '7', 8),
('17', '5', '1', 9),
('18', '5', '2', 7),
('19', '5', '4', 8),
('20', '6', '6', 9),
('21', '6', '7', 10),
('22', '6', '5', 8),
('23', '7', '4', 6),
('24', '7', '2', 4),
('25', '7', '3', 5),
('26', '8', '1', 6),
('27', '8', '7', 8),
('28', '8', '6', 4),
('29', '8', '5', 9),
('30', '9', '4', 8),
('31', '9', '9', 10);

INSERT INTO movies.movie_genre
(movie_id, genre_id)
values
('1', '1'),
('2', '2'),
('2', '1'),
('3', '3'),
('3', '2'),
('3', '1'),
('4', '2'),
('4', '1'),
('5', '2'),
('5', '1'),
('6', '3'),
('6', '4'),
('6', '1'),
('7', '2'),
('7', '1'),
('8', '5'),
('8', '1'),
('8', '6'),
('9', '3'),
('9', '4'),
('9', '7'),
('10', '1'),
('11', '3'),
('11', '8'),
('11', '2');

INSERT INTO movies.movie_country
(movie_id, country_id)
values
('1',  '1'),
('2',  '1'),
('3',  '1'),
('3',  '2'),
('4',  '1'),
('5',  '1'),
('6',  '3'),
('6',  '1'),
('7',  '1'),
('8',  '1'),
('9',  '1'),
('9',  '2'),
('10', '4'),
('10', '1'),
('11', '5');

INSERT INTO movies.movie_actor_role
(movie_id, actor_id, role_name)
values
('1',  '1', 	'Andy Dufresne'),
('1',  '2', 	'Ellis Boyd ''Red'' Redding'),
('1',  '3', 	'Warden Norton'),
('2',  '4', 	'Don Vito Corleone'),
('2',  '5', 	'Michael Corleone'),
('2',  '6', 	'Sonny Corleone'),
('2',  '12', 	'Kay Adams'),
('3',  '7', 	'Bruce Wayne'),
('3',  '8', 	'Joker'),
('3',  '9', 	'Harvey Dent'),
('4',  '5', 	'Michael'),
('4',  '10', 	'Vito Corleone'),
('4',  '11', 	'Tom Hagen'),
('4',  '12', 	'Kay'),
('5',  '13', 	'Juror 8'),
('5',  '14', 	'Juror 3'),
('5',  '15', 	'Juror 1'),
('6',  '16', 	'Frodo'),
('6',  '17', 	'Aragorn'),
('6',  '18', 	'Gandalf'),
('7',  '19', 	'Vincent Vega'),
('7',  '20', 	'Mia Wallace'),
('7',  '21', 	'Jules Winnfield'),
('8',  '22', 	'Oskar Schindler'),
('8',  '23', 	'Amon Goeth'),
('8',  '24', 	'Itzhak Stern'),
('9',  '25', 	'Cobb'),
('9',  '26', 	'Arthur'),
('9',  '27', 	'Ariadne'),
('10', '28', 	'Tyler Durden'),
('10', '29', 	'The Narrator'),
('10', '30', 	'Robert ''Bob'' Paulsen'),
('11', '31', 	'Chan Ka Kui'),
('11', '32', 	'May'),
('11', '33', 	'Selina Fong');

INSERT INTO movies.movie_director
(movie_id, director_id)
values
('1', '1'),
('2', '2'),
('3', '3'),
('4', '2'),
('5', '4'),
('6', '5'),
('7', '6'),
('8', '7'),
('9', '3'),
('10', '8'),
('11', '9');
