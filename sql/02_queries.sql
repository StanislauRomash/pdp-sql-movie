-- 01 List all the information of the actors who played a role in the movie 'MOVIE_NAME'.
select a.actor_id as "Actor ID",
	   a.first_name as "First Name",
	   a.second_name as "Second Name",
	   a.birth_date as "Birdthdate",
	   mar.role_name as "Role"
from movies.actor a
	join movies.movie_actor_role mar on a.actor_id = mar.actor_id
	        join movies.movie m on mar.movie_id = m.movie_id
where m.title = 'Fight Club'


-- 02 Write a query in SQL to list all the movies which released in the country other than UK.
select distinct m.movie_id, m.title, m."year", m.duration, m.description
from movies.movie m
where m.movie_id not in (select mc.movie_id
                         from movies.movie_country mc
                             join movies.country c on mc.country_id = c.country_id
                         where c.country_name = 'United Kingdom')
order by m.movie_id


-- 03 Write a query in SQL to find the movie title, year, date of release, director and actor for those movies which reviewer is unknown.
select m.title as "Title", m."year" as "Year", --m.release_date,
    (d.first_name || ' ' || d.second_name) as "Director",
    (a.first_name || ' ' || a.second_name) as "Actor"
from movies.movie m
    join movies.movie_director md on m.movie_id = md.movie_id
        join movies.director d on md.director_id = d.director_id
    join movies.movie_actor_role mar on m.movie_id = mar.movie_id
        join movies.actor a on mar.actor_id = a.actor_id
where m.movie_id in (select r.movie_id
                     from movies.review r
                         join movies.reviewer rr on r.reviewer_id = rr.reviewer_id
                     where rr.first_name = 'unknown')


-- 04 Write a query in SQL to find the titles of all movies directed by the director whose first and last name are FIRST_NAME LAST_NAME.
select m.title as "Title"
from movies.movie m
where m.movie_id in (select md.movie_id
                     from movies.movie_director md
                         join movies.director d on md.director_id = d.director_id
                     where d.first_name = 'Francis' and d.second_name = 'Ford Coppola')
order by m.title


-- 05 Write a query in SQL to find all the years which produced at least one movie and that received a review of more than 8 stars. Show the results in increasing order
select distinct m."year" as "Year"
from movies.movie m
    join movies.review r on m.movie_id = r.movie_id
where r.rate > 8
order by m."year"


-- 06 Write a query in SQL to find the titles of all movies that have no reviews
select m.title
from movies.movie m
where m.movie_id not in (select distinct r.movie_id
                         from movies.review r)


-- 07 Write a query in SQL to find the names of all reviewers who have reviews with a NULL value (reviewer without reviews?)
select r.reviewer_id, r.first_name, r.second_name, r.birth_date
from reviewer r
where r.reviewer_id not in (select distinct r2.reviewer_id
                            from movies.review r2)


-- 08 Write a query in SQL to find the reviewer's name and the title of the movie for those reviewers who rated more than one movies.
--    Sort the result by reviewer name, movie title, and number of stars
with active_reviewers as (
		select r2.reviewer_id
		from movies.review r2
		group by r2.reviewer_id
		having count(r2.reviewer_id) > 1
		)
select r.first_name, r.second_name, m.title, review.rate
from movies.reviewer r
    join movies.review review on r.reviewer_id = review.reviewer_id
        join movies.movie m on review.movie_id = m.movie_id
where r.reviewer_id in (select reviewer_id from active_reviewers)
order by r.first_name, r.second_name, m.title, review.rate


-- 09 Write a query in SQL to find the movie title, and the highest number of stars that movie received
--    and arranged the result according to the group of a movie and the movie title appear alphabetically in ascending order
select m.title as "Title",
       max(r.rate) as "Max rate"
from movies.movie m
    join movies.review r on m.movie_id = r.movie_id
group by m.title
order by m.title


-- 10 Write a query in SQL to find the names of all reviewers who rated the movie MOVIE_NAME.
select r.first_name, r.second_name
from movies.reviewer r
    join movies.review rev on r.reviewer_id = rev.reviewer_id
        join movies.movie m on rev.movie_id = m.movie_id
where m.title = 'The Dark Knight'
order by r.first_name


-- 11 Write a query in SQL to return the reviewer name, movie title, and number of stars for those movies which review is the lowest one.
select r2.first_name, m.title, r.rate
from movies.movie m
    join movies.review r on m.movie_id = r.movie_id
        join movies.reviewer r2 on r.reviewer_id = r2.reviewer_id
where r.rate = (select min(rate) from movies.review)
order by m.title


-- 12 Write a query in SQL to list the first and last names of all the actors who were cast in the movie 'M', and the roles they played in that production.
select a.first_name, a.second_name, mar.role_name
from movies.actor a
    join movies.movie_actor_role mar on a.actor_id = mar.actor_id
        join movies.movie m on m.movie_id = mar.movie_id
where m.title = 'Inception'


-- 13 Write a query in SQL to list all the actors who have not acted in any movie between 1990 and 2000.
select a.first_name, a.second_name, m."year"
from movies.actor a
    join movies.movie_actor_role mar on a.actor_id = mar.actor_id
        join movies.movie m on m.movie_id = mar.movie_id
where m."year" < 1990 or m."year" > 2000
order by m."year"


-- 14 Write a query in SQL to list all the movies with year, genres, and name of the director.
select m.title, m."year", g.genre_name, (d.first_name || ' ' || d.second_name) as "Director"
from movies.movie m
    join movies.movie_genre mg on m.movie_id = mg.movie_id
        join movies.genre g on mg.genre_id = g.genre_id
    join movies.movie_director md on m.movie_id = md.movie_id
        join movies.director d on md.director_id = d.director_id
order by m."year", m.title, g.genre_name


-- 15 Write a query in SQL to find those lowest duration movies along with the year, director's name, actor's name and his/her role in that production.
select m.duration as "Duration",
       m.title as "Title",
       m."year" as "Year",
       (d.first_name || ' ' || d.second_name) as "Director",
       (a.first_name || ' ' || a.second_name) as "Actor",
       mar.role_name as "Actor Role"
from movies.movie m
    join movies.movie_director md on m.movie_id = md.movie_id
        join movies.director d on md.director_id = d.director_id
    join movies.movie_actor_role mar on m.movie_id = mar.movie_id
        join movies.actor a on mar.actor_id = a.actor_id
order by m.duration


-- 16 Write a query in SQL to find movie title and number of stars for each movie that has at least one review
-- and find the highest number of stars that movie received and sort the result by movie title
select m.title as "Movie", max(r.rate) as "Max rate", count(r.review_id) as "# of Revies"
from movies.movie m
    join movies.review r on m.movie_id = r.movie_id
group by m.title
order by m.title


-- 17 Write a query in SQL to find the first and last name of an actor with their role in the movie which was also directed by themselve.
select (a.first_name || ' ' || a.second_name) as "Actor and Director",
       mar.role_name as "Role",
       m.title as "Movie Title"
from movies.movie m
    join movies.movie_actor_role mar on m.movie_id = mar.movie_id
        join movies.actor a on mar.actor_id = a.actor_id
    join movies.movie_director md on m.movie_id = md.movie_id
        join movies.director d on md.director_id = d.director_id
where a.first_name = d.first_name
  and a.second_name = d.second_name
  and a.birth_date = d.birth_date


-- 18 Write a query in SQL to find the highest-rated Mystery (Drama) movie, and report the title, year, and review
with highest_rate as (
		select max(r.rate) as max_rate, g.genre_id
		from movies.review r
		    join movies.movie_genre mg on r.movie_id = mg.movie_id
		        join movies.genre g on mg.genre_id = g.genre_id
		where g.genre_name = 'Drama'
		group by g.genre_id
		),
		movies_with_highest_rate as (
		select r.movie_id as movie_id, r.review_id as review_id
		from movies.review r
		    join movies.movie_genre mg on r.movie_id = mg.movie_id
		        join highest_rate hr on mg.genre_id = hr.genre_id and r.rate = hr.max_rate
		)
select m.title, m.year, r.review_id, r.rate
from movies.movie m
    join movies.review r on m.movie_id = r.movie_id
        join movies_with_highest_rate mhr on m.movie_id = mhr.movie_id and r.review_id = mhr.review_id
order by m.title
